#include <stdio.h>
#include <stdlib.h>


int globalWarmingQuizFunction(calculation );


int main()
{
    int quizScore;
    quizScore = globalWarmingQuizFunction();


    printf("\n\n\nYou received %d / 5", quizScore);


    if(quizScore ==5 )
    {
    printf("\n\nYour global warming knowledge is excellent!");
    }


    if(quizScore == 4)
    {
        printf("\n\nYour global warming knowledge is very good!");
    }


    if(quizScore <=3)
    {
        printf("\n\nTime to brush up on your knowledge of global warming.");
        printf("\n\nHere are some links to learn more about global warming:");
        printf("\n\nhttps://climate.nasa.gov/");
        printf("\nhttp://www.ucsusa.org/global_warming");
        printf("\nhttp://www.globalchange.gov/");
    }
}

int globalWarmingQuizFunction(calculation )
{
    int AnswerOne,AnswerTwo,AnswerThree,AnswerFour, AnswerFive;
    int globalScore = 0;

    printf("Which country currently emits the most greenhouse gases?\n\n");
    printf("1:India\t\t 2:The United Kingdom\t\t 3:U.S.\t\t 4:China\n\n");
    printf("Enter your answer(1-4):");
    scanf("%d", &AnswerOne);
    printf("Correct answer:4");
    if (AnswerOne ==4)
    {
        globalScore++;
    }
    printf("\n\n\n\nHow many human deaths per year does the World Health Organization attribute to climate change?\n\n");
    printf("1:150k\t\t 2:1.5k\t\t 3:1.5m\t\t 4:10.5k\n\n");
    printf("Enter your answer(1-4):");
    scanf("%d", &AnswerTwo);
    printf("Correct answer:1");
    if (AnswerTwo == 1)
    {
        globalScore++;
    }

    printf("\n\n\n\nWhich artic animal do many scientists consider most vulnerable to extinction due to global warming?\n\n");
    printf("1:Polar Bears\t\t 2:Toucans\t\t 3:Narwhals\t\t 4:Tropical Frogs\n\n");
    printf("Enter your answer(1-4):");
    scanf("%d", &AnswerThree);
    printf("Correct answer:3");
    if (AnswerThree == 3)
    {
        globalScore++;
    }
    printf("\n\n\n\nWhich of the following industries could be negatively affected by global warming?\n\n");
    printf("1:Insurance\t\t 2:Wine Making\t\t 3:Commercial Fishing\t\t 4:All of the above\n\n");
    printf("Enter your answer(1-4):");
    scanf("%d", &AnswerFour);
    printf("Correct answer:4");
    if (AnswerFour == 4)
    {
        globalScore++;
    }
    printf("\n\n\n\nWhich of the following types of weather-related disasters will become worse with global warming?\n\n");
    printf("1:Floods\t\t 2:Snowstorms\t\t 3:Wildfires\t\t 4:All of the above\n\n");
    printf("Enter your answer(1-4):");
    scanf("%d", &AnswerFive);
    printf("Correct answer:4");
    if (AnswerFive == 4)
    {
        globalScore++;
    }
    return globalScore;

}
